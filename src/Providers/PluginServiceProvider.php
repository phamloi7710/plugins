<?php

namespace LoiPham\Plugin\Providers;
use LoiPham\Support\Supports\Helper;
use Illuminate\Support\ServiceProvider;
use LoiPham\Plugin\Commands\PluginCreateCommand;
use LoiPham\Plugin\Commands\PluginActivateCommand;

class PluginServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->loadCommands();
    }

    public function register()
    {
        Helper::autoload(__DIR__ . '/../../helpers');
        $this->mergeConfigFrom(__DIR__.'/../../config/plugin.php', 'plugins');
        $this->app->singleton('plugins', function () {
            return true;
        });
    }
    protected function loadCommands()
    {
        $this->commands([
            PluginCreateCommand::class,
            PluginActivateCommand::class
        ]);
    }
}

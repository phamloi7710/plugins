<?php

namespace LoiPham\Plugin\Commands;

//use LoiPham\Support\Supports\SettingStore;
use Composer\Autoload\ClassLoader;
use Exception;
use File;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;

class PluginActivateCommand extends Command
{

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'plugin:activate {name : The plugin that you want to activate}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Activate a plugin in /plugins directory';

    /**
     * @var SettingStore
     */
//    protected $settingStore;

    /**
     * Create a new key generator command.
     *
     * @param \Illuminate\Filesystem\Filesystem $files
     * @param SettingStore $settingStore
     * @author Sang Nguyen
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * @throws Exception
     * @return boolean
     * @author Sang Nguyen
     */
    public function handle()
    {
        if (!preg_match('/^[a-z0-9\-]+$/i', $this->argument('name'))) {
            $this->error('Only alphabetic characters are allowed.');
            return false;
        }

        $plugin = strtolower($this->argument('name'));
        $location = plugin_path($plugin);

        if (!$this->files->isDirectory($location)) {
            $this->error('This plugin is not exists.');
            return false;
        }

        $content = get_file_data($location . '/plugin.json');
        if (!empty($content)) {
            if (!empty(Arr::get($content, 'require'))) {
                $valid = count(array_intersect($content['require'])) == count($content['require']);
                if (!$valid) {
                    $this->error('<info>Please activate plugin(s): ' . implode(',', $content['require']) . ' before activate this plugin!</info>');
                    return false;
                }
            }

            if (!class_exists($content['provider'])) {
                $loader = new ClassLoader;
                $loader->setPsr4($content['namespace'], plugin_path($plugin . '/src'));
                $loader->register(true);

                if (class_exists($content['namespace'] . 'Plugin')) {
                    call_user_func([$content['namespace'] . 'Plugin', 'activate']);
                }

                if (File::isDirectory(plugin_path($plugin . '/public'))) {
                    File::copyDirectory(plugin_path($plugin . '/public'), public_path('vendor/core/plugins/' . $plugin));

                    $this->call('vendor:publish', [
                        '--force'    => true,
                        '--tag'      => 'public',
                        '--provider' => $content['provider'],
                    ]);
                }

                if (File::isDirectory(plugin_path($plugin . '/database/migrations'))) {
                    $this->call('migrate', [
                        '--force' => true,
                        '--path'  => str_replace(base_path(), '', plugin_path($plugin . '/database/migrations')),
                    ]);
                }
            }
            $this->call('cache:clear');

            $this->line('<info>Activate plugin successfully!</info>');
        }
        return true;
    }
}

<?php
if (!function_exists('plugin_path')) {
    /**
     * @return string
     * @author Loi Pham
     */
    function plugin_path($path = null)
    {
        return __DIR__.'/../../packages'.DIRECTORY_SEPARATOR.$path;
    }
}
